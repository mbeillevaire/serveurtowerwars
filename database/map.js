function getXMLHttpRequest() {
	var xhr = null;
	
	if (window.XMLHttpRequest || window.ActiveXObject) {
		if (window.ActiveXObject) {
			try {
				xhr = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			}
		} else {
			xhr = new XMLHttpRequest(); 
		}
	} else {
		alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
		return null;
	}
	
	return xhr;
}

// type = joueur | tour
function request(carte, marqueurs) {
	var xhr = getXMLHttpRequest();

	xhr.onreadystatechange = function() {

		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
		
		//alert(xhr.responseText);
			var joueurs = xhr.responseXML.getElementsByTagName("joueur"),
				tours = xhr.responseXML.getElementsByTagName("tour"),
				marqueurs = new Array(),
				infoWindows = new Array();
				// on enregistre les vieux marqueurs pour les enlever après que les nouveaux soient mis, pour éviter le clignotement.
				//vieuxMarqueurs = marqueurs.slice();
				//alert(marqueurs.length);
			marqueurs = [];

			for(var i = 0 ; i < joueurs.length ; i++) {
				marqueurs.push(new google.maps.Marker({
					position: new google.maps.LatLng(joueurs[i].getAttribute('latitude'),
													 joueurs[i].getAttribute('longitude')),
					map: carte,
					icon: "img/joueur.png"
				}));
				//marqueurs.push(marqueur);
				infoWindows[i] = new google.maps.InfoWindow({
					content: 'joueur : ' + joueurs[i].getAttribute('name') + ', level : ' + joueurs[i].getAttribute('level') + ', points de vie : ' + joueurs[i].getAttribute('life') + ', or : ' + joueurs[i].getAttribute('gold')
				});
				google.maps.event.addListener(marqueurs[i],'click', function() {
					alert(infoWindows[i].content);
					infoWindows[i].open(carte, marqueurs[i]);
				});
			}
			for(var i = 0 ; i < tours.length ; i++) {
				var marqueur = new google.maps.Marker({
					position: new google.maps.LatLng(tours[i].getAttribute('latitude'),
					                                 tours[i].getAttribute('longitude')),
					map: carte,
					icon: "img/tour.png"
				});
				marqueurs.push(marqueur);
			}

			setTimeout(function () {
				nettoyerCarte(marqueurs);
			}, 5000);
		}
	};
	
	xhr.open("GET", "getMap.php", true);
	xhr.send(null);
}


function requestChests(carte, marqueurs) {
	var xhr = getXMLHttpRequest();

	xhr.onreadystatechange = function() {

		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			var chests = xhr.responseXML.getElementsByTagName("chest");

			for(var i = 0 ; i < chests.length ; i++) {
				var marqueur = new google.maps.Marker({
					position: new google.maps.LatLng(chests[i].getAttribute("latitude"),
													 chests[i].getAttribute("longitude")),
					map: carte,
					icon: "img/chest.png"
				});
			}
		}
	};

	xhr.open("GET", "getChests.php", true);
	xhr.send(null);
}

function nettoyerCarte(marqueurs) {
	for(var i = 0 ; i < marqueurs.length ; i++) {
		marqueurs[i].setMap(null);
	}
}
