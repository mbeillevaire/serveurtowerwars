<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">

	<head>
		<title>TowerWars - Map</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<!-- Elément Google Maps indiquant que la carte doit être affiché en plein écran et
		qu'elle ne peut pas être redimensionnée par l'utilisateur -->
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<!-- Inclusion de l'API Google MAPS -->
		<!-- Le paramètre "sensor" indique si cette application utilise détecteur pour déterminer la position de l'utilisateur -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="map.js"></script>
		<script type="text/javascript">
			function initialiser() {
				var latlng = new google.maps.LatLng(48.826152, 2.346202);
				//objet contenant des propriétés avec des identificateurs prédéfinis dans Google Maps permettant
				//de définir des options d'affichage de notre carte
				var options = {
					center: latlng,
					zoom: 14,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				
				//constructeur de la carte qui prend en paramêtre le conteneur HTML
				//dans lequel la carte doit s'afficher et les options
				var carte = new google.maps.Map(document.getElementById("carte"), options),
					marqueurs = new Array();
				
				requestChests(carte);
				request(carte, marqueurs);
				
				setInterval(function() {
					request(carte, marqueurs);
				}, 5000);
			}
		</script>
	</head>

	<body onload="initialiser()">
		<div id="carte" style="width:100%; height:100%"></div>
	</body>
</html>
