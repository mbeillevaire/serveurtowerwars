<?php 
	header ('Content-Type:text/xml');

	$usr = 'pbeillevair';
	$password = 'c3d57d6895';
	$db = new PDO('mysql:dbname=pbeillevair;host=127.0.0.1', $usr, $password, array(PDO::ATTR_PERSISTENT => true) );
	
	$requete = $db->query('SELECT * FROM chests');
	
	$chests = array();
	while($data = $requete->fetch()) {
		$chests[] = array($data['id'], $data['latitude'] / 1000000, $data['longitude'] / 1000000, $data['gold'], $data['xp']);
	}

	echo '<?xml version="1.0" encoding="UTF-8"?><chests>';
	$i = 0;
	foreach($chests as $chest) {
		echo '<chest id="', $chest[0], '" latitude="', $chest[1], '" longitude="', $chest[2], '" gold="', $chest[3], '" xp="', $chest[4], '" />';
	}

	echo '</chests>';

