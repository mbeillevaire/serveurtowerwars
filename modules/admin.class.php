<?php
	
	class admin { 
		
		public static function faire($xml, $bdd) {
			
			try {
				switch($xml->action('admin')) {
					
					case 'resetChests':
						return admin::resetChests($bdd);
						break;
					
					default:
						throw new Exception('Erreur XML : ');
				}
			
				return 0;
			} catch(Exception $e) {
				throw $e;
			}
		}
		
		private function resetChests($bdd) {
			// Sur Paris
			$startLat = 48800000;
			$startLong = 2240000; 
			
			$endLat = 48908036;
			$endLong = 2421181;
			/*
			// Sur la France
			$startLat = 42666308;
			$startLong = -4399934; 
			
			$endLat = 51193138;
			$endLong = 7926725;
			*/
			//total nbr of chests : nbrChestsLat * nbrChestsLong
			$nbrChestsLat = 20;
			$nbrChestsLong = 20;
			
			//average distance btwn chests
			$avgDistLat = ($endLat - $startLat) / $nbrChestsLat;
			$avgDistLong = ($endLong - $startLong) / $nbrChestsLong;
			
			// Dans [0,1] :
			$AmpRandLat = 0.4;
			$AmpRandLong = 0.4;
			
			//max gains :
			$maxGold = 100;
			$maxXp = 200;
			
			$sql = 'DELETE FROM chests;' . "\n";
			$sql.= 'INSERT INTO chests (id, latitude, longitude, gold, xp) VALUES ' . "\n";
			
			for($lat = $startLat ; $lat < $endLat ; $lat += $avgDistLat) {
				for($long = $startLong ; $long < $endLong ; $long += $avgDistLong) {
					
					//random nbrs in ]-1,1[
					$randLat = 1 - $AmpRandLat * rand(0, 199) / 100;
					$randLong = 1 - $AmpRandLong * rand(0, 199) / 100;
					
					$latChest = $lat + floor($randLat * $avgDistLat);
					$longChest = $long + floor($randLong * $avgDistLong);
					
					//Generating xp & gold (btwn 10% and 100% of maxGold/maxXp)
					$goldChest = rand($maxGold / 10, $maxGold);
					$xpChest = rand($maxXp / 10, $maxXp);
					
					$data = '(\'\', ' . $latChest . ', ' . $longChest . ', ' . $goldChest . ', ' . $xpChest . '), ' . "\n";
					
					$sql .= $data;
				}
			}
			
			$sql = substr($sql, 0, -3) . ';';
			
			try {
				return 'Reset : ' . (adminSql::resetChests($sql, $bdd) > 1 ? 'ok' : 'fail...');
			} catch (Exception $e) {
				throw $e;
			}
		}
		
		
		
		
	}

?>
