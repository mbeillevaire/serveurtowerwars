<?php
class Tour
{
	private $id;
	private $owner;
	private $position;
	private $life;
	private $blocs;
	private $attaquesRestantesBlocD;
	private $xpApportee;
	
	const DEGATS = 10;
	const RAYON_ATTAQUE = 1; //en km
	
	public function __construct($data, $db = null, $fromSakatour = false) {
		
		if($db != null) { //Creation du joueur a partir de la db. $db = instance pdo
			
			if((int)$data['id'] > 0) {
				
				$this->id = (int)$data['id'];
				
				if($fromSakatour) {
					$reponse = $db->getTourByIdFromSakatour($this->id);
				} else {
					$reponse = $db->getTourById($this->id);
				}
				
				if($donnees = $reponse->fetch()) {
				
					$this->owner =					$donnees['owner'];
					$this->position =				new Coordonnees($donnees['longitude'], $donnees['latitude']);
					$this->blocs =					$donnees['blocs'];
					$this->life =					$donnees['pointsDeVie'];
					$this->attaquesRestantesBlocD = $donnees['attaquesRestantesBlocD'];
					
				} else {
					throw new Exception('Erreur tour : aucune tour pour l\'id ' . (int)$data['id'] . '.');
				}
			
			} else
				throw new Exception('Erreur tour : "id" non valable.');
			
		} else { //on utilise le xml pour construire la tour
			
			foreach($data as $key => $value) {
				
				switch($key) {
					case 'id':	$this->id = $data['id']; break;
					case 'owner': $this->owner = $data['owner']; break;
					case 'latitude':
					case 'longitude':
						$pos = new Coordonnees($data['longitude'], $data['latitude']);
						$this->setPosition($pos); 
						break;
						
					case 'pointsDeVie': $this->setLife($data['pointsDeVie']); break;
					case 'blocs': $this->setBlocs($data['blocs']); break;
					default:
						//throw new Exception('Donnée inconnue : ' . $key . '. Seuls les attributs id, owner, latitude, longitude, pointsDeVie sont autorisés');
				}
			}
			
		}
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getOwner()
	{
		return $this->owner;
	}
	
	public function getLevel()
	{
		return $this->level;
	}

	public function getRayon()
	{
		return $this->rayon;
	}
	
	public function getLife()
	{
		return $this->life;
	}
	
	public function getBlocs()
	{
		return $this->blocs;
	}
	
	public function getPosition()
	{
		return $this->position;
	}
	
	public function getAttaquesRestantesBlocD() {
		return $this->lvlBlocsDefense;
	}
	
	
	public function setOwner($owner)
	{
		$this->owner = $owner;
	}
	
	public function setLevel($lvl)
	{
		if((int)$lvl > 0) {
			$this->level = (int)$lvl;
			return true;
		}
		return false;
	}
	
	public function setRayon($rayon)
	{
		if((int)$rayon > 0) {
			$this->rayon = $rayon;
			return true;
		} else {
			$this->rayon = 100;
			return false;
		}
	}
	
	public function setLife($life)
	{
		if($life > 0) {
			if($life < 100) {
				$this->life = $life;
				return true;
			} else {
				$this->life = 100;
			}
		} else {
			$this->life = 0;
		}
		return false;
	}
	
	public function setPosition($pos) {
		$this->position = $pos;
	}
	
	public function setBlocs($blocs) {
		if(preg_match("#^[ard]+$#i", $blocs)) {
			$this->blocs = $blocs;
			return true;
		}
		return false;
	}
	
	public function setAttaquesRestantesBlocD($lvlBlocsD) {
		if($lvlBlocsD >= 0) {
			$this->lvlBlocsDefense = (int)$lvlBlocsD;
			return true;
		}
		
		return false;
	}
	
	public function attaquer($joueur, $gestionJoueurs, $accesBdd) {
		
		if($joueur->getPointsDeVie() > 0 && $joueur->getOnline() == 1) { //le joueur est vivant et loggué
			if($this->estProcheJoueur($joueur, self::RAYON_ATTAQUE)) {
				$owner = new Player(array('id' => $this->getOwner()), $accesBdd);
				
				$req = $accesBdd->getPlayerById($joueur->getId());
				$arrayJoueur = $req->fetch();
				$life = $arrayJoueur['pointsDeVie'];
				
				if($this->getOwner() != $joueur->getId()) {
					
					if(true) { // Condition si on réussit l'attaque (vrai tout le temps pour le moment)
						
						// dégats = Const * level blocs A * nbre de blocs A dans la tour
						$degats = self::DEGATS * $owner->getLvlABloc() * substr_count($this->blocs, 'A');
						
						$joueur->augmenterPointsDeVie(-$degats);
						
						//update du joueur dans la bdd :
						if($gestionJoueurs->update($joueur, array('pointsDeVie'))) {
							return $joueur->getPointsDeVie();
						} else {
							return -1;
						}
						
					} else {
						//raté
					}
				} else {
					return -2;
				}
			} else {
				return -3;
			}
		} else {
			return -5;
		}
	}
	
	
	
	public function getInfosTour()
	{
		return $this->blocs.'/'.$this->position->getX().'/'.$this->position->getY().'/'.$this->owner.'/'.$this->life.'/';
	}
	
	private function estProcheJoueur(Player $joueur, $distanceMin = self::RAYON_ATTAQUE) {
		
		$diffLong = $joueur->getPosition()->getX() - $this->position->getX();
		$diffLat = $joueur->getPosition()->getY() - $this->position->getY();
		
		$diffLong = deg2rad($diffLong / 1000000);
		$diffLat = deg2rad($diffLat / 1000000);
		
		//Carre de la dist. en mètres
		$cosinus = cos(deg2rad($this->position->getY() / 1000000));
		$distCarre = 40576900 * ($diffLat * $diffLat + $cosinus * $cosinus * $diffLong * $diffLong);
		
		return ($distCarre < $distanceMin * $distanceMin);
	}
	
	
	
}
?>
