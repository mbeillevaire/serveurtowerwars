<?php

	class TowerWars {
		
		public static function faire($texteXml, $bdd) {
			
			try {
				
				$xml = new TowerWarsXmlDocument($texteXml);
				
				//fonctionnalités admin
				if($xml->isAdmin()) {
					return admin::faire($xml, $bdd);
				}
				
				$gestionJoueurs = new GestionJoueurs($bdd);
				$gestionTours = new GestionTours($bdd);
				
				switch($xml->action()) {
					case 'login':
						//$_SESSION['connected'] = 0;
						$infosJoueur = $xml->infosJoueur();
						$passwd = $infosJoueur['password'];
						$joueur = new Player($xml->infosJoueur(), new accesSql($bdd));
						if($joueur->login($passwd) == 1) {
							//$_SESSION['connected'] = 1;
							return $gestionJoueurs->login($joueur);
						} else {
							return 0;
						}
					case 'nouveauJoueur':
						$joueur = self::creatingNewPlayer($xml->infosJoueur(), $hashedPasswd, $salt); 
						return $joueur == null ? -2 : $gestionJoueurs->ajouter($joueur, $hashedPasswd, $salt);
				
					case 'obtenirInfosTour':
						$accesBdd = new accesSql ($bdd);
						$tour = new Tour ($xml->infosTour(), $accesBdd);
						return $tour->getInfosTour();
						
					case 'tourAttaqueJoueur':
						$accesBdd = new accesSql($bdd);
						$joueur = new Player($xml->infosJoueur(), $accesBdd);
						$tour = new Tour($xml->infosTour(), $accesBdd);
						return $tour->attaquer($joueur, $gestionJoueurs, $accesBdd);
					
					case 'obtenirInfosJoueur':
						$accesBdd = new accesSql($bdd);
						$joueur = new Player($xml->infosJoueur(), $accesBdd);
						return $joueur->getInfosJoueur();
					
					case 'supprimerTour':
						$infosTour = $xml->infosTour();
						return $gestionTours->supprimmer($infosTour['id']);
					
					default:
						$infosJoueur = $xml->infosJoueur();
						$accesBdd = new accesSql($bdd);
						if(/*isset($_SESSION['connected']) && $_SESSION['connected'] == 1 && */self::isLogged($infosJoueur, $accesBdd)) {
							
							switch ($xml->action()) {
								
								case 'logout':
									//$_SESSION = array();
									$joueur = new Player($infosJoueur);
									return $gestionJoueurs->logout($joueur);
								
								case 'mettreAJourJoueur':
									$joueur = new Player($infosJoueur, $accesBdd);
									$arrayUpdate = array_intersect(array_keys($infosJoueur), array('latitude', 'longitude', 'lvlABloc', 'lvlDBloc', 'lvlRBloc'));
									return $gestionJoueurs->update($joueur, $arrayUpdate, true);
									
								case 'mettreDansSakatour':
									$joueur = new Player($infosJoueur, $accesBdd);
									$tour = new Tour($xml->infosTour());
									return $joueur->dansSakatour($tour, $gestionTours, $gestionJoueurs);
								
								case 'poserTour':
									$joueur = new Player($infosJoueur, $accesBdd);
									$tour = new Tour($xml->infosTour(), $accesBdd, true);
									return $joueur->poserDepuisSakatour($tour, $gestionTours, $gestionJoueurs, $accesBdd);
									
								case 'getTowers':
									$joueur = new Player($infosJoueur);
									return $joueur->getTours($accesBdd);
									
								case 'nouvelleTour':
									$joueur = new Player($infosJoueur, $accesBdd);
									$tour = new Tour($xml->infosTour());
									
									return $joueur->nouvelleTour($tour, $gestionTours, $gestionJoueurs, $accesBdd);
									
								case 'obtenirMapJoueurs':
									$joueur = new Player($infosJoueur, $accesBdd);
									return $joueur->getMapJoueurs($accesBdd);
									
								case 'obtenirMapTours':
									$joueur = new Player($infosJoueur, $accesBdd);
									return $joueur->getMapTours($accesBdd);
									
								case 'joueurAttaqueTour': 
									$joueur = new Player($infosJoueur, $accesBdd);
									$tour = new Tour($xml->infosTour(), $accesBdd);
									return $joueur->attaquer($tour, $gestionTours, $gestionJoueurs, $accesBdd);
								
								case 'supprimerJoueur':
									return $gestionJoueurs->supprimmer($infosJoueur['id']);
									
								case 'obtenirMapCoffres':
									$joueur = new Player($infosJoueur, $accesBdd);
									return $joueur->getMapChests($accesBdd);

								case 'ouvrirCoffre':
									$idChest = $xml->attribut($xml->balise('chest'), 'id');
									$joueur = new Player($infosJoueur, $accesBdd);
									return $joueur->ouvrirCoffre($accesBdd, $gestionJoueurs, $idChest);
								
								default:
									throw new Exception('No action specified...');
								
							}
						} else {
							throw new Exception('Not logged...');
						}
				}
			} catch(Exception $e) {
				exit($e->getMessage());
			}
		}
		
		public static function creatingNewPlayer($infosJoueur, &$hashedPasswd, &$salt) {
			
			$name = $infosJoueur['name'];
			$passwd = $infosJoueur['password'];
			
			if(strlen($name) >= 2 && strlen($passwd) >= 6) {
			
				//creating the salt :
				$chars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
				$final_rand='';
				for($i=0 ; $i < 20 ; $i++) { //salt = 20 chars.
					$final_rand .= $chars[ rand(0,36)];
				}
				
				$salt = $final_rand;
				$hashedPasswd = sha1($salt . $passwd);
				
				unset($infosJoueur['password']);
				$newPlayer = new Player($infosJoueur);
				
				return $newPlayer;
			}
			
			return null;
		}
		
		public static function isLogged($infosJoueur, $accesBdd) {
			$sqlJoueur = $accesBdd->getPlayerById($infosJoueur['id']);
			if($sqlJoueur == null) {
				return false;
			}
			
			while($data = $sqlJoueur->fetch()) {
				return ($data['online'] == 1);
			}
			
			return false;
		}
	}

?>
