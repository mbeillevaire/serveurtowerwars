<?php
	
	class gestionJoueurs {
		
		private $bdd; //instance PDO	
		
		public function __construct($bdd) {
			$this->bdd = $bdd;
		}
		
		public function ajouter(Player $joueur, $hashedPasswd, $salt) {
			
			$requeteSQL = 'INSERT INTO joueurs VALUES(\'\', :name, \'' . $hashedPasswd . '\', \'' . $salt . '\', 0, NOW(), NOW(), 0, 0, 100, 1, 0, 10000, 1, 1, 1, 0)';
			
			if(!$this->joueurExiste($joueur->getName())) {
				
				$req = $this->bdd->prepare($requeteSQL);
				if($req->execute( array('name' => $joueur->getName()) )) {
					return $this->bdd->lastInsertId();
				} else {
					return -1;
				}
			} else {
				return -3;
			}
		}
		
		public function supprimmer($idJoueur) {
			
			$req = $this->bdd->prepare('DELETE FROM joueurs WHERE id=:idJoueur;
										DELETE FROM tours WHERE owner=:idJoueur;');
			return $req->execute(array('idJoueur' => (int)$idJoueur)); //true si ok, false sinon.
		}
		
		public function update(Player $joueur, $modifiedData, $updateGold = false) {
			
			$data = array('id' => (int)$joueur->getId());

			if($joueur->getLastTimeLifeUp() != '') { // si on dispose d'un temps de maj des pdv
				
				//On update les pts de vie (mais que si on ne l'a pas fait depuis quelques temps , ie +10pts / 1h = 3600s)
				$lifePerHour = 10;
				$diffTime = time() - strtotime($joueur->getLastTimeLifeUp());
				
				$diffNbreUp = (int)($diffTime / 3600);

				if($diffNbreUp > 0) {
					$joueur->augmenterPointsDeVie($lifePerHour * $diffNbreUp);
					$modifiedData[] = 'pointsDeVie';
					if($joueur->getPointsDeVie() == 100) {
						$modifiedData[] = 'lastTimeLifeUp';
						$data['lastTimeLifeUp'] = date('Y-m-d H:i:s');
					} else {
						$modifiedData[] = 'lastTimeLifeUp';
						$data['lastTimeLifeUp'] = date('Y-m-d H:i:s', strtotime($joueur->getLastTimeLifeUp()) + 7200 * $diffNbreUp);
					}
				}
			}
			
			
			if($updateGold) { // on update l'or
				
				//Update des ressources :
				$this->getRessources($joueur, $gold, $lastTimeUp, $nbreBlocsR, $lvlRBloc);
				
				$A = 1 * $lvlRBloc;  //coeffs pour les nouvelles ressources : newGold += A * ancienGold + B
				$B = 0.1;
				
				$diffTime = time() - strtotime($lastTimeUp);
				
				$newGold = $gold + $diffTime * ($A * $nbreBlocsR + $B);
				
				$joueur->setGold($newGold);
				
				$modifiedData[] = 'goldAmount';
			}
			
			//Update des attributs du joueur dans la db :
			$requeteSQL = '';
			
			if(count($modifiedData) > 0) {
				
				$updatedXp = 0;
				
				foreach($modifiedData as $champ) {
					switch($champ) {
						case 'lvlABloc':
						case 'lvlDBloc':
						case 'lvlRBloc':
							$methode = 'get' . ucfirst($champ);
							if($joueur->$methode() > 0) {
								
								$newLvl = ($joueur->$methode()) + 1;
								$prix = 10000 * $newLvl;
								
								
								if($prix <= $joueur->getGoldAmount()) {
									$joueur->setGold($joueur->getGoldAmount() - $prix);
									
									$updatedXp += 35;
									
									$requeteSQL .= $champ . '=' . $newLvl . ', ';
									$achatEffectue = true;
								}
							}
							break;
							
						case 'online':
						case 'lastTimeUp':
						case 'pointsDeVie':
						case 'blocsR':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$methode = 'get' . ucfirst($champ);
							$data[$champ] = $joueur->$methode();
							break;
						
						case 'decrementBlocsR':
							$requeteSQL .= 'blocsR=blocsR-1, ';
							break;
							
						case 'latitude':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$data[$champ] = $joueur->getPosition()->getY();
							break;
						case 'longitude':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$data[$champ] = $joueur->getPosition()->getX();
							break;
						case 'goldAmount':
							$requeteSQL .= 'goldAmount=:goldAmount, ';
							$data['goldAmount'] = $joueur->getGoldAmount();
							break;
						case 'xpCount':
							$requeteSQL .= 'xpCount=:xpCount, ';
							$data['xpCount'] = $joueur->getXpCount();
							$updatedXp += $joueur->getXpCount();
							break;
						case 'level':
							$requeteSQL .= 'level=:level, ';
							$data['level'] = $joueur->getLevel();
							break;
						case 'lastTimeLifeUp':
							$requeteSQL .= 'lastTimeLifeUp=:lastTimeLifeUp, ';
					}
				}
				
				if($updateGold) {
					$requeteSQLFinale = 'UPDATE joueurs SET goldAmount=' . $joueur->getGoldAmount() . ', xpCount=xpCount+' . $updatedXp . ', lastTimeUp=NOW() WHERE id=:id;' . "\n";
				}
				if($requeteSQL != '') {
					$requeteSQLFinale .= 'UPDATE joueurs SET ' . substr($requeteSQL, 0, -2) . ' WHERE id=:id;';
				}
				if($updatedXp > 0) {
					$requeteSQLFinale .= "\n" . 'UPDATE joueurs SET xpCount=xpCount-100*level, level=level+1 WHERE xpCount >= 100*level;';
				}
				
				if($requeteSQLFinale != '') {
					$req = $this->bdd->prepare($requeteSQLFinale);
					if($req->execute($data)) {
							
							if($achatEffectue) {
								return (($newGold - $prix) / 100 ). '/' . (($A * $nbreBlocsR + $B) / 100);
							} else if($updateGold) {
								return ($newGold / 100) . '/' . (($A * $nbreBlocsR + $B) / 100);
							} else {
								return 1;
							}
					
					} else {
						return -1;
					}
				} else {
					return 0;
				}
				
			} else { // si on ne fait que mettre à jour l'or
				if($updateGold) {
					$req = $this->bdd->prepare($requeteSQLFinale);
					
					if($req->execute($data)) {
						
						return ($newGold / 100) . '/' . (($A * $nbreBlocsR + $B) / 100);
					
					} else {
						return -1;
					}
				} else {
					return 0;
				}
			}
		}
		
		public function joueurExiste($name) {
			
			return ($this->bdd->query('SELECT count(*) FROM joueurs WHERE name=\'' . $name . '\'')->fetchColumn() != 0);
		}
		
		public function login($joueur) {
			
			if($this->update($joueur, array('online'))) {
				return $joueur->getInfosJoueur(true); //true pour afficher l'id au lieu du nom.
			} else {
				return -1;
			}
		}
		
		public function logout($joueur) {
			$joueur->logout();
			return $this->update($joueur, array('online'), false);
		}
		
		private function getRessources($joueur, &$gold, &$lastTimeUp, &$nbreBlocsR, &$lvlRBloc) {
			
			if($req = $this->bdd->query('SELECT goldAmount, lastTimeUp, blocsR, lvlRBloc FROM joueurs WHERE id=' . (int)$joueur->getId())) {
				$data = $req->fetch();
				
				$gold = $data['goldAmount'];
				$lastTimeUp = $data['lastTimeUp'];
				$nbreBlocsR = $data['blocsR'];
				$lvlRBloc = $data['lvlRBloc'];
				
				return true;
			}
			
			return false;
			
		}
		
	}
?>
