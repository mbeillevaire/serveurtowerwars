<?php

	class accesSql {
		
		private $bdd;
		
		public function __construct($bdd) {
			$this->bdd = $bdd;
		}
		
		public function getTours($avecJoueurs = false) {
			if($avecJoueurs) {
				return $this->bdd->query('SELECT t.id id, t.longitude longitude, t.latitude latitude, t.owner owner, t.blocs blocs, t.hauteur hauteur, j.name nameOwner FROM tours t INNER JOIN joueurs j ON j.id = t.owner');
			} else {
				return $this->bdd->query('SELECT * FROM tours');
			}
		}	
		
		public function getJoueurs() {	
			return $this->bdd->query('SELECT * FROM joueurs');
		}
		
		public function getOnlinePlayers() {
			return $this->bdd->query('SELECT * FROM joueurs WHERE online=1');
		}
		
		public function getTourById($id) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT * FROM tours WHERE id=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function getTourByIdFromSakatour($id) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT * FROM sakatour WHERE id=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function getToursByOwnerFromSakatour($id) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT * FROM sakatour WHERE owner=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function getToursByOwner($idOwner) {
			if((int)$idOwner > 0) {
				return $this->bdd->query('SELECT * FROM tours WHERE owner=' . (int)$idOwner);
			} else {
				return null;
			}
		}
		
		public function getPlayerById($id) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT * FROM joueurs WHERE id=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function getPlayerByName($name) {
			if($name != '') {
				return $this->bdd->query('SELECT * FROM joueurs j WHERE j.name=\'' . htmlentities($name) . '\'');
			} else {
				return null;
			}
		}
		
		public function getInfoOnJoueurById($id, $field) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT ' . $field . ' FROM joueurs WHERE id=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function getChests($lat = 0, $long = 0, $delta = 0) {
			if($delta != 0) {
				$dLong = $delta; //en milionnième de degré.
				$dLat = $delta;
			
				$minLong = $long - $dLong;
				$minLat = $lat - $dLat;
			
				$maxLong = $long + $dLong;
				$maxLat = $lat + $dLat;
			
				$request = 'SELECT * FROM chests WHERE longitude < ' . $maxLong . ' AND longitude > ' . $minLong . ' AND latitude < ' . $maxLat . ' AND latitude > ' . $minLat;
				return $this->bdd->query($request);
			} else {
				$request = 'SELECT * FROM chests';
				return $this->bdd->query($request);
			}
		}
			
		public function getChestById($id) {
			if((int)$id > 0) {
				return $this->bdd->query('SELECT * FROM chests WHERE id=' . (int)$id);
			} else {
				return null;
			}
		}
		
		public function deleteChest($id) {
			$req = $this->bdd->prepare('DELETE FROM chests WHERE id=:id');
			return $req->execute(array('id' => (int)$id));
		}
	}
	
