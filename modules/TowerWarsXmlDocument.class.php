<?php
	
	class TowerWarsXmlDocument extends DomDocument {
		
		private $admin;
		private $action;
		private $infosJoueur;
		private $infosTour;
		
		public function __construct($texteXml) {
			parent::__construct();
			
			try {
				if(!@$this->loadXML($texteXml)) {
					throw new Exception('Exception XML : le code n\'est pas valide XML.
Voil&agrave; le xml : ' . $texteXml);
				} else {
					if($this->balise('admin') == true) {
						$this->admin = true;
					}
				}
				
			} catch(Exception $e) {
				if($e->getCode() == 0) {
					throw $e;
				} else {
					$admin = false;
				}
			}
		}
		
		//3 méthodes pour avoir accès aux balises et aux attributs
		public function balise($nom) {
			$nomBalise = (String)$nom;
			
			$listeBalises = $this->getElementsByTagName($nomBalise);
			
			if($listeBalises != null) {
				return $listeBalises->item(0);
			} else if($nom == 'admin') {
				throw new Exception('Exception XML : pas de balise ' . $nomBalise, 1);
			} else {
				throw new Exception('Exception XML : pas de balise ' . $nomBalise, 0);
			}
		}
		
		public function listeAttributs($balise) {
			$liste = array();
			
			if($balise != null && $balise->attributes != null) {
				foreach($balise->attributes as $nom => $valeur) {
					$liste[$nom] = $valeur->nodeValue;
				}
				return $liste;
			} else {
				throw new Exception('Exception XML : balise sans attribut ou null.');
			}
		}
		
		public function attribut($balise, $nom) {
			
			$nomAttr = (String)$nom;
			
			if($balise != null && $balise->attributes->getNamedItem($nomAttr) != null) {
				return $balise->attributes->getNamedItem($nomAttr)->nodeValue;
			} else {
				throw new Exception('Exception XML : pas d\'attribut "' . $nomAttr .'" dans la balise');
			}
		}
		
		public function setAction($balise) {
			try {
				$baliseAction = $this->balise($balise);
				$this->action = $this->attribut($baliseAction, 'type');
			} catch (Exception $e) {
				throw $e;
			}
		}
		
		public function setJoueur() {
			try {
				$baliseJoueur = $this->balise('joueur');
				$this->infosJoueur = $this->listeAttributs($baliseJoueur);
			} catch (Exception $e) {
				throw $e;
			}
		}
		
		public function setTour() {
			try {
				$baliseTour = $this->balise('tour');
				$this->infosTour = $this->listeAttributs($baliseTour);
			} catch (Exception $e) {
				throw $e;
			}
		}
		
		
		public function action($balise = 'action') {
			$this->setAction($balise);
			return $this->action;
		}
		
		public function infosJoueur() {
			$this->setJoueur();
			return $this->infosJoueur;
		}
		
		public function infosTour() {
			$this->setTour();
			return $this->infosTour;
		}
		
		public function isAdmin() {
			return $this->admin;
		}
	}
	
?>
