<?php
class Coordonnees
{
	private $x;
	private $y;
	
	public function __construct($x, $y) {
		$intX = (int)$x;
		$intY = (int)$y;
		
		//limitation des angles : -180° < longitude < 180 ° et -90° < latitude < 90° (en millionième de degré)
		//Si pas correct : coordonnées par défault (Télécom)
		if($intX > -180000000 && $intX < 180000000) {
			$this->x = $intX;
		} else {
			$this->x = 2346111;
		}
		if($intY > -90000000 && $intY < 90000000) {
			$this->y = $intY;
		} else {
			$this->y = 48826045;
		}
	}
	
	public function getX()
	{
		return $this->x;
	}
	
	public function getY()
	{
		return $this->y;
	}
	
	public static function setCoordonnees($nouveauX,$nouveauY)
	{
		$intX = (int)$nouveauX;
		$intY = (int)$nouveauY;
		$returnX = false;
		$returnY = false;
		
		//limitation des angles : -180° < longitude < 180 ° et -90° < latitude < 90° (en millionième de degré)
		if($intX > -180000000 && $intX < 180000000) {
			$this->x = $intX;
			$returnX = true;
		}
		if($intY > -90000000 && $intY < 90000000) {
			$this->y = $intY;
			$retrunY = true;
		}
		
		return $returnX && $returnY;
	}
	
}
?>
