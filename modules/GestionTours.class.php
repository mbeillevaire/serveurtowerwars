<?php
	
	class GestionTours {
		

		
		private $bdd; //instance PDO	
		
		public function __construct($bdd) {
			$this->bdd = $bdd;
		}
		
		public function ajouter(Tour $tour) {
			
			$requeteSQL = 'INSERT INTO tours (id, owner, latitude, longitude, pointsDeVie, hauteur, blocs, attaquesRestantesBlocD) 
							SELECT \'\', :owner, :y, :x, 100, :hauteur, :blocs, j.lvlDBloc - 1 FROM joueurs j WHERE j.id=:owner;';
			
			$data['owner'] = $tour->getOwner();
			$data['x'] = $tour->getPosition()->getX();
			$data['y'] = $tour->getPosition()->getY();
			$data['hauteur'] = strlen($tour->getBlocs());
			$data['blocs'] = $tour->getBlocs();
						
			$req = $this->bdd->prepare($requeteSQL);
			if($req->execute($data)) {
				return $this->bdd->lastInsertId();
			} else {
				return 0;
			}
		}
		
		public function ajouterSakatour(Tour $tour) {
			
			$requeteSQL = 'INSERT INTO sakatour (id, owner, blocs, atkRestantesD) 
							SELECT \'\', :owner, :blocs, j.lvlDBloc - 1 FROM joueurs j WHERE j.id=:owner;';
			
			$data['owner'] = $tour->getOwner();
			$data['blocs'] = $tour->getBlocs();
			
			$req = $this->bdd->prepare($requeteSQL);
			if($req->execute($data)) {
				return $this->bdd->lastInsertId();
			} else {
				return 0;
			}
		}
		
		public function supprimmer($idTour) {
			
			//On met à jour le nbre de blocsR du joueur qui possède la tour, puis on casse tout.
			$req = $this->bdd->prepare('UPDATE joueurs j
										INNER JOIN tours t ON j.id=t.owner
										SET j.blocsR = j.blocsR - (LENGTH(t.blocs) - LENGTH(REPLACE(t.blocs, "R", "")))
										WHERE t.id=:idTour;
										
										DELETE FROM tours WHERE id=:idTour;');
			
			return $req->execute(array('idTour' => (int)$idTour));
		}
		
		public function supprimmerDeSakatour($idTour) {
			$req = $this->bdd->prepare('DELETE FROM sakatour WHERE id=:idTour;');
			return $req->execute(array('idTour' => (int)$idTour));
		}
		
		public function update(Tour $tour, $modifiedData) {
			
			$requeteSQL = '';
			
			if(count($modifiedData) > 0&& $tour->getId() > 0) {
				
				$data = array('id' => $tour->getId());
				
				foreach($modifiedData as $champ) {
					switch($champ) {
						case 'owner':
						case 'blocs':
						case 'attaquesRestantesBlocD':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$methode = 'get' . ucfirst($champ);
							$data[$champ] = $tour->$methode();
							break;
							
						case 'longitude':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$data['longitude'] = $tour->getPosition()->getX();
							break;
						case 'latitude':
							$requeteSQL .= $champ . '=:' . $champ . ', ';
							$data['latitude'] = $tour->getPosition()->getY();
							break;
						case 'resetAttaquesBlocsD':
							$resetAttaquesBlocsD = true;
					}
				}
				
				if($resetAttaquesBlocsD) {
					$requeteSQL = 'UPDATE tours AS t
									JOIN joueurs AS j ON j.id=t.owner
									SET t.attaquesRestantesBlocD=j.lvlDBloc,
									' . substr($requeteSQL, 0, -2) . ' WHERE t.id=:id';
				} else {
					//(substr est là pour enlever la dernière virgule qui ne sert à rien)
					$requeteSQL = 'UPDATE tours SET ' . substr($requeteSQL, 0, -2) . ' WHERE id=:id';
				} 
				
				$req = $this->bdd->prepare($requeteSQL);
				
				return $req->execute($data); //renvoie true si ok, false sinon.
				
			}
		}
		
	}
?>
