<?php

	class chest {
	
		private $id;
		private $position;
		private $xp;
		private $gold;
		
		public function __construct($data, accesSql $db = null) {
		
			if($db != null) { //Creation du coffre a partir de la db. $db = instance pdo
			
				if(isset($data['id']) && $data['id'] > 0) { 
					$this->id = (int)$data['id'];
					$reponse = $db->getChestById($this->id);
				} else {
					return 0;
				}
			
				while($donnees = $reponse->fetch()) {
					$this->id = 			$donnees['id'];
					$this->position = 		new Coordonnees($donnees['longitude'], $donnees['latitude']);
					$this->xp = 			$donnees['xp'];
					$this->goldAmount = 	$donnees['gold'];
				}
			
				$reponse->closeCursor();
				$reponse = null;
			
				//Modifications du joueur suivant le xml
				foreach($data as $key => $value) {
			
					switch($key) {
						case 'id':	$this->id = $value; break;
						case 'latitude':
						case 'longitude':
							$pos = new Coordonnees($data['longitude'], $data['latitude']);
							$this->setPosition($pos); 
							break;
					}
				}
			} else { //on utilise seulement le xml pour construire le joueur
				foreach($data as $key => $value) {
				
					switch($key) {
						case 'id':	$this->id = (int)$value; break;
						case 'latitude':
						case 'longitude':
							$pos = new Coordonnees((int)$data['longitude'], (int)$data['latitude']);
							$this->setPosition($pos); 
							break;
					
						case 'xp': $this->setXp($value); break;
						case 'gold': $this->setGold($value); break;
						default:
							//throw new Exception('Donnée inconnue : ' . $key . '. Seuls les attributs id, name, latitude, longitude, pointsDeVie, level, xp, gold et lvl*Bloc avec * = A, D ou R sont autorisés');
					}
				}
			}
		}
		
		public function getId() {
			return $this->id;
		}
		
		public function getPosition() {
			return $this->position;
		}
		
		public function getXp() {
			return $this->xp;
		}
		
		public function getGold() {
			return $this->gold;
		}
		
		public function setPosition(Coordonnees $position) {
			$this->position = $position;
			return true;
		}
		
		public function setXp($nouveauXp)
		{
			if((int)$nouveauXp >= 0) {
				$this->xp = (int)$nouveauXp;
				return true;
			}
			return false;
		}
	
		public function setGold($newGold)
		{
			if((int)$newGold >= 0) {
				$this->goldAmount = (int)$newGold;
				return true;
			}
			return false;
		}
	
	
	
	
	}
