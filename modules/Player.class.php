<?php
class Player
{
	const MAX_XP = 100;
	const MAX_LIFE = 100;
	const MAX_LVLBLOCS = 5;
	
	const DEGATS = 10;
	//distances en km :
	const DISTANCE_VISION = 10;
	/*
	const DISTANCE_MIN_TOURS = 0.2;*/
	const DISTANCE_MIN_TOURS = 0;
	const RAYON_ATTAQUE = 0.2;
	const CHEST_DETECTION = 0.1;
	
	private $id; 
	private $playerName;
	private $hashedPasswd;
	private $salt;
	private $lastTimeLifeUp;
	private $online;
	private $position;
	private $life;
	private $level;
	private $xp;
	private $goldAmount;
	private $lvlABloc; // Niveau d'amélioration du bloc d'attaque
	private $lvlDBloc; // du bloc défense
	private $lvlRBloc; // du bloc ressource
	private $blocsR;
	
	
	public function __construct($data, accesSql $db = null) {
		
		if($db != null) { //Creation du joueur a partir de la db. $db = instance pdo
			
			if(isset($data['id']) && $data['id'] > 0) { 
				$this->id = (int)$data['id'];
				$reponse = $db->getPlayerById($this->id);
			} else if(isset($data['name']) && $data['name'] != '') {
				$this->name = $data['name'];
				$reponse = $db->getPlayerByName($this->name);
			} else {
				return 0;
			}
			
			while($donnees = $reponse->fetch()) {
				$this->id = 			$donnees['id'];
				$this->playerName = 	$donnees['name'];
				$this->hashedPasswd = 	$donnees['passwd'];
				$this->salt = 			$donnees['salt'];
				$this->online = 		$donnees['online'];
				$this->lastTimeLifeUp = $donnees['lastTimeLifeUp'];
				$this->position = 		new Coordonnees($donnees['longitude'], $donnees['latitude']);
				$this->life = 			$donnees['pointsDeVie'];
				$this->level = 			$donnees['level'];
				$this->xp = 			$donnees['xpCount'];
				$this->goldAmount = 	$donnees['goldAmount'];
				$this->lvlABloc =	 	$donnees['lvlABloc'];
				$this->lvlDBloc =	 	$donnees['lvlDBloc'];
				$this->lvlRBloc =	 	$donnees['lvlRBloc'];
				$this->blocsR =			$donnees['blocsR'];
			}
			
			$reponse->closeCursor();
			$reponse = null;
			
			//Modifications du joueur suivant le xml
			foreach($data as $key => $value) {
			
				switch($key) {
					case 'id':	$this->id = $value; break;
					case 'latitude':
					case 'longitude':
						$pos = new Coordonnees($data['longitude'], $data['latitude']);
						$this->setPosition($pos); 
						break;
					case 'lvlABloc':
					case 'lvlDBloc':
					case 'lvlRBloc':
						if($value == 0) {
							$methode = 'set' . ucfirst($key);
							$this->$methode(0);
						}
				}
			}
		} else { //on utilise seulement le xml pour construire le joueur
			foreach($data as $key => $value) {
				
				switch($key) {
					case 'id':	$this->id = (int)$value; break;
					case 'name': $this->playerName = $value; break;
					
					case 'latitude':
					case 'longitude':
						$pos = new Coordonnees((int)$data['longitude'], (int)$data['latitude']);
						$this->setPosition($pos); 
						break;
					
					case 'pointsDeVie': $this->setPointsDeVie($value); break;
					case 'level': $this->setLevel($value); break;
					case 'xp': $this->setXpCount($value); break;
					case 'gold': $this->setGoldAmount($value); break;
					case 'lvlABloc': $this->setLvlABloc($value); break;
					case 'lvlDBloc': $this->setLvlDBloc($value); break;
					case 'lvlRBloc': $this->setLvlRBloc($value); break;
					default:
						//throw new Exception('Donnée inconnue : ' . $key . '. Seuls les attributs id, name, latitude, longitude, pointsDeVie, level, xp, gold et lvl*Bloc avec * = A, D ou R sont autorisés');
				}
			}
		}
	}
	
	//---------- Getters -----------
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getOnline()
	{
		return $this->online;
	}
	
	public function getName()
	{
		return $this->playerName;
	}
	
	public function getPosition()
	{
		return $this->position;
	}
	
	public function getLevel()
	{
		return $this->level;
	}
	
	public function getPointsDeVie()
	{
		return $this->life;
	}
	
	public function getXpCount()
	{
		return $this->xp;
	}
	
	public function getGoldAmount()
	{
		return $this->goldAmount;
	}
	
	public function getLvlABloc()
	{
		return $this->lvlABloc;
	}
	
	public function getLvlDBloc()
	{
		return $this->lvlDBloc;
	}
	
	public function getLvlRBloc()
	{
		return $this->lvlRBloc;
	}
	
	public function getBlocsR()
	{
		return $this->blocsR;
	}
	
	public function getLastTimeLifeUp() {
		return $this->lastTimeLifeUp;
	}
	
	
	//---------- Setters -----------
	
	public function setPosition(Coordonnees $position) {
		$this->position = $position;
		return true;
	}
	
	public function setOnline($online) {
		if($online == 0 || $online == 1) {
			$this->online = (int)$online;
			return true;
		}
		return false;
	}
	
	public function setPointsDeVie($newLife)
	{
		if($newLife >= 0 && $newLife <= self::MAX_LIFE) {
			$this->life = $newLife;
			return $this->life;
		} else if($newLife < 0) {
			$this->life = 0;
			return 0;
		} else if($newLife > self::MAX_LIFE) {
			$this->life = self::MAX_LIFE;
			return self::MAX_LIFE;
		} else {
			return false;
		}
	}	
	
	//augmente les points de vie si paramètre > 0, diminue si le param est négatif.
	public function augmenterPointsDeVie($augmentation) {	
		return $this->setPointsDeVie($this->life + (int)$augmentation);
	}
	
	public function setLevel($nouveauLevel)
	{
		if((int)$nouveauLevel > 0) {
			$this->level = (int)$nouveauLevel;
			return true;
		}
		return false;
	}
	
	public function setXpCount($nouveauXp)
	{
		if((int)$nouveauXp >= 0) {
			$this->xp = (int)$nouveauXp;
			return true;
		}
		return false;
	}
	
	public function setGold($newGold)
	{
		if((int)$newGold >= 0) {
			$this->goldAmount = (int)$newGold;
			return true;
		}
		return false;
	}
	
	public function setLvlABloc($lvl)
	{
		if((int)$lvl >= 0) {
			$this->lvlABloc = (int)$lvl;
			return true;
		}
		return false;
	}
	
	public function setLvlDBloc($lvl)
	{
		if((int)$lvl >= 0) {
			$this->lvlDBloc = (int)$lvl;
			return true;
		}
		return false;
	}
	
	public function setLvlRBloc($lvl)
	{
		if((int)$lvl >= 0) {
			$this->lvlRBloc = (int)$lvl;
			return true;
		}
		return false;
	}
	
	public function setBlocsR($nbreBlocsR) {
		
		if((int)$nbreBlocsR >= 0) {
			$this->blocsR = (int)$nbreBlocsR;
			return true;
		} 
		return false;
	}
	
	
	public function setLastTimeLifeUp($date) {
		if(preg_match('[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}')) {
			$this->lastTimeLifeUp = $date;
			return true;
		}
		return false;
	}
	
	//------------------------------
	
	public function login($passwd) {
		
		if(sha1($this->salt . $passwd) == $this->hashedPasswd) {
			if($this->setOnline(1)) {
				return 1;
			} else {
				return false;
			}
		}
		return 0;
	}
	
	public function logout() {
		return $this->setOnline(0);
	}
	
	/*
	 * Retourne les blocs de la tour, ou bien 0 si rien n'a été effectué (trop loin, ...)
	 */
	public function attaquer($tower, $gestionTours, $gestionJoueurs, $accesBdd)
	{
		if($this->estProcheTour($tower, self::RAYON_ATTAQUE)) {
			
			$req = $accesBdd->getTourById($tower->getId());
			$tableauTour = $req->fetch();
			$blocs = $tableauTour['blocs'];
			
			if($tower->getOwner() != $this->getId()) {
				
				$owner = new Player(array('id' => $tower->getOwner()), $accesBdd);
			
				if(true) { // Condition si on réussit l'attaque (vrai tout le temps pour le moment)
					
					// Dernier bloc : on supprime la tour
					if(strlen($blocs) == 1) {
						$tourSuppr = $gestionTours->supprimmer($tower->getId());
					} else {
						$tourSuppr = 0;
					}

					//s'il n'y a pas de blocs défense
					if(strpos($blocs, 'D')===false) {
						$i=rand(0,strlen($blocs)-1);
						
						if($blocs[$i] == 'R') {
							$gestionJoueurs->update($owner, array('decrementBlocsR'));
							$methode = 'getLvlRBloc';
						} else {
							$methode = 'getLvlABloc';
						}
						
						while ($i < (strlen($blocs)-1)) { 
							$blocs[$i]=$blocs[$i+1]; 
							$i++;
						}
						$blocs = substr($blocs, 0, -1);
						
						$xpGagnee = 20 * $owner->$methode();
						$this->xp += $xpGagnee;
						$gestionJoueurs->update($this, array('xpCount'));
						
					} else { // s'il y a un bloc défense
						$i=strpos($blocs, 'D');
						$lvlBlocsDefense = $tableauTour['attaquesRestantesBlocD'];
						
						if($lvlBlocsDefense == 0) {
							while ($i < (strlen($blocs)-1)) { 
								$blocs[$i]=$blocs[$i+1];
								$i++;
							}
							$blocs = substr($blocs, 0, -1);
							$resetAttaquesBlocsD = true;
							
							$xpGagnee = 20 * $owner->getLvlDBloc();
							$this->xp += $xpGagnee;
							$gestionJoueurs->update($this, array('xpCount'));
							
						} else {
							$tower->setAttaquesRestantesBlocD($lvlBlocsDefense - 1);
							$xpGagnee = 0;
						}
					}
				}
				else { /*raté*/ }
				
				$tower->setBlocs($blocs);
				$arrayModifiedData = ($resetAttaquesBlocsD == true ? array('blocs', 'resetAttaquesBlocsD'): array('blocs', 'attaquesRestantesBlocD'));
				
				if( $gestionTours->update($tower, $arrayModifiedData) ) {
					//on renvoie 1 ou 0 si la tour a bien été suppr ou non lorsque le dernier bloc a été détruit.
					return ($blocs != '' ? $blocs : $tourSuppr) . '/' . $xpGagnee;
				} else {
					return '-1/0';
				}
			} else {
				return '-2/0';
			}
		}
		
		return '-3/0';
	}
	
	/*
	 * Deprecated ---> Sakatour naow !
	 */
	public function nouvelleTour(Tour $tour, GestionTours $gestionTours, GestionJoueurs $gestionJoueurs, accesSql $accesBdd) {	
		
		$nbreBlocs = strlen($tour->getBlocs());
		
		$nbreBlocsR = substr_count($tour->getBlocs(), 'R');
		$nbreBlocsD = substr_count($tour->getBlocs(), 'D');
		$nbreBlocsA = substr_count($tour->getBlocs(), 'A');
		
		/*
		 * Le joueur a un lvl assez grand pour le nombre de blocs ?
		 */
		if(4 + floor($this->getLevel() / 2) < $nbreBlocs) {
			return -3;
		}
		
		/*
		 * Le joueur est assez riche ?
		 */
		$prixTour =  $nbreBlocs * 100;
		
		if($this->getGoldAmount() < $prixTour) {
			return -1;
		}
		
		
		/*
		 * Vérification des distances entre tours
		 */
		$dataTours = $accesBdd->getTours(true);
		$goldTested = false;
		
		while($data = $dataTours->fetch()) {
			$tourCourante = new Tour($data);
			if($this->estProcheTour($tourCourante, self::DISTANCE_MIN_TOURS)) {
				return -2; //si on est trop proche d'une tour, ça ne marche pas.
			}
		}
		
		//On construit la tour :
		$tour->setPosition($this->position);
		$tour->setOwner($this->id);
		$tour->setLife(100);
		
		//maj du joueur
		$this->setBlocsR($this->getBlocsR() + $nbreBlocsR);
		$this->setGold($this->getGoldAmount() - $prixTour);
		
		$newXp = $this->getXpCount() + 10 * ($nbreBlocsA + $nbreBlocsD + $nbreBlocsR);
		$newLevel = $this->getLevel();
		while($newXp >= 100 * $newLevel) {
			$newXp -= 100 * $newLevel;
			$newLevel++;
		}
		$this->setLevel($newLevel);
		$this->setXpCount($newXp);
		
		if($gestionJoueurs->update($this, array('blocsR', 'xpCount', 'level', 'goldAmount'))) {
			return $gestionTours->ajouter($tour);
		} else {
			return 0;
		}
	}
	
	public function getInfosJoueur($displayIdInsteadOfName = false) {
	
		$idOrName = ($displayIdInsteadOfName ? $this->id : $this->playerName);
		
		return $idOrName.
		'/'.$this->position->getY().
		'/'.$this->position->getX().
		'/'.$this->level.
		'/'.$this->life.
		'/'.$this->xp.
		'/'.$this->goldAmount.
		'/'.$this->lvlABloc.
		'/'.$this->lvlDBloc.
		'/'.$this->lvlRBloc.'/';
	 
	}
	
	public function getMapJoueurs($accesBdd) {
		
		$joueurs = $accesBdd->getOnlinePlayers();
		
		$return = '';
		while($joueur = $joueurs->fetch()) {
			
			if($this->estProcheJoueur(new Player($joueur), self::DISTANCE_VISION)) {
				$return .= $joueur['id'] .
				'/'.$joueur['name'] .
				'/'.$joueur['longitude'] .
				'/'.$joueur['latitude'] .
				'/'.$joueur['level'] .
				'/'.$joueur['pointsDeVie'] .
				'/'.$joueur['xpCount'] .
				'/'.$joueur['goldAmount'] .
				'/'.$joueur['lvlABloc'] .
				'/'.$joueur['lvlDBloc'] .
				'/'.$joueur['lvlRBloc'] . '/' . "\n";
				
			}
		}
		
		return ($return == '' ? '0' : $return);
	}
	
	public function getMapTours($accesBdd) {
		
		$tours = $accesBdd->getTours(true);
		$return = '';
		
		while($tour = $tours->fetch()) {
			
			if($this->estProcheTour(new Tour($tour), self::DISTANCE_VISION)) {
				$return .= $tour['id'] .
					'/'.$tour['blocs'] .
					'/'.$tour['longitude'] .
					'/'.$tour['latitude'] .
					'/'.$tour['hauteur'] .
					'/'.$tour['owner'] .
					'/'.$tour['nameOwner'] . '/' . "\n";
			}
		}
		
		return ($return == '' ? '0' : $return);
	}
	
	public function getMapChests($accesBdd) {
		
		$chests = $accesBdd->getChests($this->position->getY(), $this->position->getX(), 20000);
		$return = '';
		
		while($chest = $chests->fetch()) {
			
			$return .= $chest['id'] .
				'/'.$chest['latitude'] .
				'/'.$chest['longitude'] . '/';
		}
		
		return ($return == '' ? '0' : $return);
	}
	
	public function dansSakatour(Tour $tour, GestionTours $gestionTours, GestionJoueurs $gestionJoueurs) {
		
		$nbreBlocs = strlen($tour->getBlocs());
		
		$nbreBlocsR = substr_count($tour->getBlocs(), 'R');
		$nbreBlocsD = substr_count($tour->getBlocs(), 'D');
		$nbreBlocsA = substr_count($tour->getBlocs(), 'A');
		
		/*
		 * Le joueur a un lvl assez grand pour le nombre de blocs ?
		 * Il faut lvl >= 2*(nbre blocs - 4)  <==> 4 + ent(lvl/4) >= nbre blocs.
		 */
		if($this->getLevel() < 2 * ($nbreBlocs - 4)) {
			return -3;
		}
		
		//On construit la tour :
		$tour->setOwner($this->id);
		
		return $gestionTours->ajouterSakatour($tour);
	}
	
	public function poserDepuisSakatour(Tour $tour, GestionTours $gestionTours, GestionJoueurs $gestionJoueurs, accesSql $accesBdd) {
		
		/*
		 * Le joueur est assez riche ?
		 */
		$prixTour = strlen($tour->getBlocs()) * 2000;
		
		if($this->getGoldAmount() <= $prixTour) {
			return -2;
		}
		
		/*
		 * Vérification des distances entre tours
		 */
		$dataTours = $accesBdd->getTours(true);
		
		while($data = $dataTours->fetch()) {
			$tourCourante = new Tour($data);
			if($this->estProcheTour($tourCourante, self::DISTANCE_MIN_TOURS)) {
				return -3; //si on est trop proche d'une tour, ça ne marche pas.
			}
		}
		
		//On construit la tour :
		$tour->setPosition($this->position);
		$tour->setOwner($this->id);
		$tour->setLife(100);

		$nbreBlocsR	= substr_count($tour->getBlocs(), 'R');

		//maj du joueur
		$this->setBlocsR($this->getBlocsR() + $nbreBlocsR);
		$this->setGold($this->getGoldAmount() - $prixTour);

		if($gestionJoueurs->update($this, array('blocsR', 'goldAmount'))) {
			$gestionTours->supprimmerDeSakatour($tour->getId());
			return $gestionTours->ajouter($tour);
		} else {
			return -1;
		}
	} 
	
	public function getTours($accesBdd) {
		
		$sakatour = $accesBdd->getToursByOwnerFromSakatour($this->id);
		
		$return = '';
		while($tour = $sakatour->fetch()) {
			$return .= $tour['id'] . '/' . $tour['blocs'] . '/';
		}
		
		$return .= '*';
		
		$tours = $accesBdd->getToursByOwner($this->id);
		
		while($tour = $tours->fetch()) {
			
			$return .= $tour['id'] .
				'/'.$tour['blocs'] .
				'/'.$tour['longitude'] .
				'/'.$tour['latitude'] .
				'/'.$tour['hauteur'] . '/';
		}
		
		return $return;
		
	}

	public function ouvrirCoffre($accesBdd, $gestionJoueurs, $idChest) {

		$xp = 0;
		$gold = 0;
		
		if($this->testChest($accesBdd, $idChest, $xp, $gold)) {

			$this->setGold($this->goldAmount + $gold);
			$this->setXpCount($this->xp + $xp);
			
			if($gestionJoueurs->update($this, array('goldAmount', 'xpCount'))) {
				if($accesBdd->deleteChest($idChest) ) {
					return $xp . '/' . $gold;
				} else {
					return -1;
				}
			} else {
				return -2;
			}
		}
		return 0;
	}
	
	//test s'il y a un coffre dans le coin.
	private function testChest($accesBdd, $idChest, &$xp = 0, &$gold = 0) {
		
		//$reqSql = $accesBdd->getChests($this->position->getY(), $this->position->getX(), 5000);
		$reqSql = $accesBdd->getChestById($idChest);
		$chest = $reqSql->fetch();
		
		if($this->estProche($chest['latitude'], $chest['longitude'], self::CHEST_DETECTION)) {
			$xp = $chest['xp'];
			$gold = $chest['gold'];
			return 1;
		}

		return 0;
	}
	
	/*
	 * Les 2 fonctions suivantes renvoient true si le joueur ou la tour est proche du joueur courant ($this),
	 * ou false s'ils sont éloignés.
	 */ 
	private function estProcheTour(Tour $tour, $distanceMin = self::RAYON_ATTAQUE) {
		$diffLong = $tour->getPosition()->getX() - $this->position->getX(); // En milionnièmes de deg
		$diffLat = $tour->getPosition()->getY() - $this->position->getY();
		
		/* Carre de la dist. en mètres, car ||dM||² = (Rdθ)² + (Rsin(Pi/2 - θ)dφ)²
		 * et R_t² = 40.577 Mega metres^2, arrondi à 41
		 * et sin(Pi/2 - θ) = cos(θ)
		 */
		$diffLong = deg2rad($diffLong / 1000000);
		$diffLat = deg2rad($diffLat / 1000000);
		
		//Carre de la dist. en mètres
		$cosinus = cos(deg2rad($this->position->getY() / 1000000));
		$distCarre = 40576900 * ($diffLat * $diffLat + $cosinus * $cosinus * $diffLong * $diffLong);
		
		return ($distCarre < $distanceMin * $distanceMin);
	}
	
	private function estProcheJoueur(Player $joueur, $distanceMin = self::RAYON_ATTAQUE) {
		$diffLong = $joueur->getPosition()->getX() - $this->position->getX();
		$diffLat = $joueur->getPosition()->getY() - $this->position->getY();
		
		$diffLong = deg2rad($diffLong / 1000000);
		$diffLat = deg2rad($diffLat / 1000000);
		
		//Carre de la dist. en mètres
		$cosinus = cos(deg2rad($this->position->getY() / 1000000));
		$distCarre = 40576900 * ($diffLat * $diffLat + $cosinus * $cosinus * $diffLong * $diffLong);
		
		return ($distCarre < $distanceMin * $distanceMin);
	}
	
	private function estProche($lat, $long, $distanceMin = self::RAYON_ATTAQUE) {
		
		$diffLong = $long - $this->position->getX();
		$diffLat = $lat - $this->position->getY();
		
		$diffLong = deg2rad($diffLong / 1000000);
		$diffLat = deg2rad($diffLat / 1000000);
		
		//Carre de la dist. en mètres
		$cosinus = cos(deg2rad($this->position->getY() / 1000000));
		$distCarre = 40576900 * ($diffLat * $diffLat + $cosinus * $cosinus * $diffLong * $diffLong);
		
		return ($distCarre < $distanceMin * $distanceMin);
	}
}

?>
