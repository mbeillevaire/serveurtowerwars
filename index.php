<?php
	//On démarre tout ce qui est utile :
	//sessions
	session_start();
	/*
	echo "cookies : ";
	print_r($_COOKIES);
	echo "\n\nsession : ";
	print_r($_SESSION);
	echo "\n session id : ", session_id(), "\n";
	echo "\n session name : ", session_name(), "\n";
	*/
	//database
	$usr = 'pbeillevair';
	$password = 'c3d57d6895';
	$database = new PDO('mysql:dbname=pbeillevair;host=127.0.0.1', $usr, $password, array(PDO::ATTR_PERSISTENT => true) );
	
	//Auto-lanceur de classes 
	function autoload($class) {
		include 'modules/' . $class . '.class.php';
	}
	spl_autoload_register('autoload');
	
	if(isset($_POST['xml']) && $_POST['xml'] != '') {
		$xml = stripslashes(str_replace('%20', ' ', $_POST['xml']));
	}
	
	if(isset($_GET['xml']) && $_GET['xml'] != '') {
		$xml = stripslashes(str_replace('%20', ' ', $_GET['xml']));
	}
	
	if(isset($xml)) {
		$retour = TowerWars::faire($xml, $database);
		
		//On retourne ce qu'il faut :
		echo htmlentities($retour);
		
	} else {
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Tour Power 3000</title>
        <link rel="stylesheet" media="screen" type="text/css" title="Design" href="design.css" />
    </head>

    <body>
    	<div class="strut"></div><p><img src="homeImg.jpg" /></p>
    </body>
</html>

<?php
	}
